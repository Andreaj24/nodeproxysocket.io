var express = require('express');
var router = express.Router();
var conf = require('../config.json');

/* GET home page. */
router.get('/', function (req, res) {
    res.render('index', {
        title: 'Node Socket proxy'
    });
});


router.get('/proxy/notification/:userId', _securityAccess, function (req, res) {
    var _message = req.query['message'] || undefined;

    req.io.sockets.emit('event:ping:user:' + req.params.userId, {
        message: JSON.parse(_message)
    });

    res.status(200).json({
        status: 'success',
        message: 'notification sent',
        object: {
            userId: req.params.userId,
            message: JSON.parse(_message)
        }
    })
});

function _securityAccess(req, res, next) {
    if (req.query['token'] == conf.secert) return next();
    else res.status(401).json({
        error: true,
        message: 'Tocken error'
    });
}

module.exports = router;
