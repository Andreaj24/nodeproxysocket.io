## NodeJs Proxy for socket.io
----

#Usage

```

sudo npm install
npm start

```

#API
---

**BaseURL** : http://ip:port

| VERB| PATH| QUERY PARAMS|
|--|--| --|
|GET | /proxy/notification/:USERID| message=MESSAGGIO & token=ACCESS TOKEN| 



```

Example : http://localhost:3000/proxy/notification/12345?message=%22Ciaomessaggiodi20prova%22&token=**token**
```

#EXAMPLE
---

Open Example/test.html

See the log for the ping connection

Call the Example API, see the alert with message